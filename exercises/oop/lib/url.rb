# frozen_string_literal: true

# BEGIN
class Url
  require 'uri'
  require 'forwardable'
  extend Forwardable
  include Comparable
  def_delegators :@uri, :scheme, :host, :to_s

  def initialize(url_str)
    @uri = URI(url_str)
  end

  def <=>(other)
    to_s <=> other.to_s
  end

  def query_params
    @uri.query.split('&')
        .map { |pair| pair.split('=') }
        .to_h.transform_keys(&:to_sym)
  end

  def query_param(key, default = nil)
    query_params[key] || default
  end
end
# END
