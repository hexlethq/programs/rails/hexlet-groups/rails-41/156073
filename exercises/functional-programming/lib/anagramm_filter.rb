# frozen_string_literal: true

def anagramm_filter(word, words)
  chars = word.chars.sort
  words.select { |w| w.chars.sort == chars }
end
