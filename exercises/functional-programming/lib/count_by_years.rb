# frozen_string_literal: true

def count_by_years(users)
  res = {}
  users.each do |user|
    next unless user[:gender] == 'male'

    year = user[:birthday].split('-').first
    res[year] ||= 0
    res[year] += 1
  end

  res
end
