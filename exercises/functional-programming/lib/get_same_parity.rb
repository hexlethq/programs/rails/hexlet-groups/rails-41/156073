# frozen_string_literal: true

def get_same_parity(list)
  return [] if list[0].nil?

  even = list[0].even?
  list.select { |el| el.even? == even }
end
