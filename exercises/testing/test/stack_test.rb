# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  def test_pop
    stack = Stack.new([1, 2, 3])
    pop_result = stack.pop!
    assert pop_result == 3
    assert stack.to_a == Stack.new([1, 2]).to_a

    stack = Stack.new
    pop_result = stack.pop!
    assert_nil pop_result
    assert_empty stack
  end

  def test_push
    stack = Stack.new
    stack.push! 1
    assert stack.to_a == Stack.new([1]).to_a
  end

  def test_empty
    stack = Stack.new
    assert_empty stack
  end

  def test_to_a
    array = [1, 2, 3]
    stack = Stack.new(array)
    assert_equal stack.to_a, array
  end

  def test_clear
    stack = Stack.new([1, 2, 3])
    stack.clear!
    assert_empty stack
  end

  def test_size
    array = [1, 2, 3]
    stack = Stack.new(array)
    assert_equal stack.size, array.size
  end
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
