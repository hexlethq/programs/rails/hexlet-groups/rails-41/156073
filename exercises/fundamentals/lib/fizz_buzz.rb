# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  (start..stop).map do |i|
    word = ''
    word += 'Fizz' if (i % 3).zero?
    word += 'Buzz' if (i % 5).zero?
    word.empty? ? i.to_s : word
  end.join(' ')
end
# END
