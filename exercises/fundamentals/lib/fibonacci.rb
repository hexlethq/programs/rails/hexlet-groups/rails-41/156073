# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil if num.negative?
  return 0 if num < 2

  previous = 0
  current = 1
  (3..num).each do
    current += previous
    previous = current - previous
  end

  current
end
# END
