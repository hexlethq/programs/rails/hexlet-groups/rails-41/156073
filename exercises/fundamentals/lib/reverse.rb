# frozen_string_literal: true

# BEGIN
def reverse(str)
  str.chars.inject([]) { |a, i| a.unshift(i) }.join
end
# END
