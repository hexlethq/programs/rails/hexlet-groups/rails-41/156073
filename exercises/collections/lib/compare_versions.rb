# frozen_string_literal: true

def compare_versions(version1, version2)
  return 0 if version1 == version2

  version1_arr = version_to_array(version1)
  version2_arr = version_to_array(version2)
  version1_arr.each_with_index do |num, index|
    return 1 if version2_arr.size < index + 1 || num > version2_arr[index]
    return -1 if num < version2_arr[index]
  end

  version1_arr.size == version2_arr.size ? 0 : -1
end

def version_to_array(version)
  version.split('.').map(&:to_i)
end
