# frozen_string_literal: true

def build_query_string(hash)
  hash.sort.map { |key, value| "#{key}=#{value}" }.join('&')
end
